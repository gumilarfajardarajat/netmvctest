using MAIN;
using MAIN.Controllers;
using MAIN.Entities;
using MAIN.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Xunit.Abstractions;

namespace XTEST
{
    public class UnitTest
    {
        private ApplicationDbContext db;
        private TransactionService transactionService;
        private HomeController homeController;
        private DbContextOptions<ApplicationDbContext> _dbContextOptions;
        protected readonly ITestOutputHelper output;

        public UnitTest(ITestOutputHelper output)
        {
            _dbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().Options;
            db = new ApplicationDbContext(_dbContextOptions);
            this.output = output;
            transactionService = new TransactionService(db);
            homeController = new HomeController();
            

            
        }

        [Fact]
        public void GetList()
        {
            var list = transactionService.GeneratePersonal();
            var cnt = list.Count();
            var res = JsonConvert.SerializeObject(list);
            output.WriteLine(res);
            output.WriteLine(cnt.ToString());
        }

        [Fact]
        public void AddService()
        {
            var jsonString = "[{\"Id\":1,\"Nama\":\"John\",\"IdGender\":1,\"IdHobi\":3,\"Umur\":25},{\"Id\":2,\"Nama\":\"Alice\",\"IdGender\":2,\"IdHobi\":2,\"Umur\":30},{\"Id\":3,\"Nama\":\"Michael\",\"IdGender\":1,\"IdHobi\":1,\"Umur\":22},{\"Id\":4,\"Nama\":\"Emily\",\"IdGender\":2,\"IdHobi\":4,\"Umur\":28},{\"Id\":5,\"Nama\":\"David\",\"IdGender\":1,\"IdHobi\":5,\"Umur\":35}]";
            var personals = JsonConvert.DeserializeObject<IEnumerable<Personal>>(jsonString) ?? Enumerable.Empty<Personal>();
            var res = transactionService.Add(personals);
            Assert.NotNull(res);
            
            
        }

        [Fact]
        public void MigrateAge()
        {
            transactionService.MigrateAge();
        }

        [Fact]
        public void AddController()
        {
            var jsonString = "[{\"Id\":1,\"Nama\":\"John\",\"IdGender\":1,\"IdHobi\":3,\"Umur\":25},{\"Id\":2,\"Nama\":\"Alice\",\"IdGender\":2,\"IdHobi\":2,\"Umur\":30},{\"Id\":3,\"Nama\":\"Michael\",\"IdGender\":1,\"IdHobi\":1,\"Umur\":22},{\"Id\":4,\"Nama\":\"Emily\",\"IdGender\":2,\"IdHobi\":4,\"Umur\":28},{\"Id\":5,\"Nama\":\"David\",\"IdGender\":1,\"IdHobi\":5,\"Umur\":35}]";
            //var personals = JsonConvert.DeserializeObject<IEnumerable<Personal>>(jsonString) ?? Enumerable.Empty<Personal>();
            //Assert.IsAssignableFrom<IEnumerable<Personal>>(personals);
            var res = homeController.AddPersonals(jsonString);
            Assert.IsAssignableFrom<OkObjectResult>(res);
        }

    }
}