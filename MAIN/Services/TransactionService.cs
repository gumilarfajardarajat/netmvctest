﻿using MAIN.Entities;
using Microsoft.EntityFrameworkCore;


namespace MAIN.Services
{
    public class TransactionService
    {
        private ApplicationDbContext db;
        public TransactionService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public IEnumerable<Personal> Add(IEnumerable<Personal> personals)
        {
            db.Personal.AddRange(personals);
            db.SaveChanges();
            MigrateAge();
            return personals;
        }

        public void MigrateAge()
        {
            db.Database.ExecuteSqlRaw("DELETE FROM tblT_Umur");
            db.Database.ExecuteSqlRaw(@"INSERT INTO tblT_Umur(Umur,Total) SELECT Umur, COUNT(1) AS Total FROM tblT_Personal GROUP BY Umur");
        }

        public IEnumerable<Personal> GeneratePersonal()
        {
            List<Personal> list = new List<Personal>();

            for (int i = 0; i < 1000; i++)
            {
                Random rand = new Random();
                int minAge = 18;
                int maxAge = 40;
                int randomAge = rand.Next(minAge, maxAge + 1);

                const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

                string randomString = new string(Enumerable.Repeat(chars, 25)
                  .Select(s => s[rand.Next(s.Length)]).ToArray());

                var gender = db.Genders.FromSqlRaw(@"SELECT TOP 1 * FROM tblM_gender ORDER BY NEWID()").First();
                var hobi = db.Hobbies.FromSqlRaw(@"SELECT TOP 1 * FROM tblM_Hobi ORDER BY NEWID()").First();
                var lastId = 0;
               
                var count = db.Personal.FromSqlRaw(@"SELECT TOP 1 * FROM tblT_Personal ORDER BY ID DESC").Count();
                if (count > 0)
                {
                    var lastPersonal = db.Personal.FromSqlRaw(@"SELECT TOP 1 * FROM tblT_Personal ORDER BY ID DESC").First();
                    lastId = lastPersonal.Id;
                }
                else {
                    lastId = 1;
                }
                

                lastId = lastId + i + 1;

                var personal = new Personal()
                {
                    Id = lastId,
                    Nama = randomString,
                    IdGender = gender.Id,
                    IdHobi = hobi.Id,
                    Umur = randomAge
                };

                list.Add(personal);

            }
            
            return list;

        }

    }
}
