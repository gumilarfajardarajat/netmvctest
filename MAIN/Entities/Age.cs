﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAIN.Entities
{
    [Table("tblT_Umur")]
    public class Age
    {
        [Key]
        public int Id { get; set; }
        public string Nama { get; set; }
    }
}
