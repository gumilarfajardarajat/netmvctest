﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAIN.Entities
{
    [Table("tblM_gender")]
    public class Gender
    {
        [Key]
        public int Id { get; set; }
        public string Nama { get; set; }
    }
}
