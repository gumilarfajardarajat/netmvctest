﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAIN.Entities
{
    [Table("tblM_Hobi")]
    public class Hobby
    {
        [Key]
        [StringLength(1)]
        [Column(TypeName = "char")]
        public string Id { get; set; }
        public string Nama { get; set; }
    }
}
