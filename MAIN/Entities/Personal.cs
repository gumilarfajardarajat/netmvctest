﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MAIN.Entities
{
    [Table("tblT_Personal")]
    public class Personal
    {
        [Key]
        public int Id { get; set; }
        public string Nama { get; set; }
        public int IdGender { get; set; }
        [StringLength(1)]
        [Column(TypeName = "char")]
        public string IdHobi { get; set; }
        public int Umur { get; set; }
    }
}
