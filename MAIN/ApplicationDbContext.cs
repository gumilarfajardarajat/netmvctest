﻿using MAIN.Entities;
using Microsoft.EntityFrameworkCore;

namespace MAIN
{
    public class ApplicationDbContext : DbContext
    {

        public virtual DbSet<Age> Ages { get; set; }
        public virtual DbSet<Hobby> Hobbies { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Personal> Personal { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=ROBOT\\SQLEXPRESS;Initial Catalog=ASTEK;Integrated Security=True;Connect Timeout=30;Encrypt=True;Trust Server Certificate=True;Application Intent=ReadWrite;Multi Subnet Failover=False");
                optionsBuilder.EnableSensitiveDataLogging();
            }
        }
    }
}
