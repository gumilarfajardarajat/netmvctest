using MAIN.Entities;
using MAIN.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;


namespace MAIN.Controllers
{
    public class HomeController : Controller
    {
        
        private TransactionService transactionService { get; set; }

        public HomeController()
        {
            
            var dbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().Options;
            var db = new ApplicationDbContext(dbContextOptions);
            transactionService = new TransactionService(db);
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("AddPersonals")]
        public IActionResult AddPersonals([FromBody] List<Personal> personals)
        {
            try
            {
                if (personals == null || personals.Count == 0)
                {
                    return BadRequest("JSON data is empty.");
                }

                var res = transactionService.Add(personals);

                return Ok(personals);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [Route("GeneratePersonals")]
        public IActionResult GeneratePersonals()
        {
            try
            {
                var result = transactionService.GeneratePersonal();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
